<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 12.05.17
 * Time: 13:07
 */
namespace app\models;
use yii\db\Query;
use yii\db\Expression;

class Customer extends AbstractUserType
{
    const TYPE = 2;

    public function getBalance()
    {
        $connection = \Yii::$app->getDb();
        $command = $connection->createCommand(
            "SELECT SUM(balance_transactions.amount) as summary
                FROM balance_transactions
                INNER JOIN users ON users.id = balance_transactions.user_id
                WHERE users.parent_id={$this->id};"
        );
        $result = $command->queryOne();
        return $result['summary'];

    }

    public function getManagers()
    {
        return $this->hasMany(Manager::className(), ['parent_id' => 'id']);
    }

}