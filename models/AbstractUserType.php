<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 12.05.17
 * Time: 14:23
 */

namespace app\models;

abstract class AbstractUserType extends User implements UserAbstractionInterface
{
    const TYPE = 0;

    public static function find()
    {
        return new UserQuery(get_called_class(), ['type' => static::TYPE]);
    }

    public function beforeSave($insert)
    {
        $this->type = static::TYPE;
        return parent::beforeSave($insert);
    }
}