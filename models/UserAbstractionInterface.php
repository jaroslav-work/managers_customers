<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 12.05.17
 * Time: 13:11
 */
namespace app\models;
interface UserAbstractionInterface {
    public function getBalance();
}