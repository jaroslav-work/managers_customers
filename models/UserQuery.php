<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 12.05.17
 * Time: 14:17
 */
namespace app\models;

use yii\db\ActiveQuery;

class UserQuery extends ActiveQuery {
    public $type;

    public function prepare($builder)
    {
        if ($this->type !== null) {
            $this->andWhere(['type' => $this->type]);
        }
        return parent::prepare($builder);
    }
}
