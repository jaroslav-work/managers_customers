<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 12.05.17
 * Time: 13:07
 */
namespace app\models;
class Manager extends AbstractUserType {
    const TYPE = 1;
    public function getBalance()
    {
        $connection = \Yii::$app->getDb();
        $command = $connection->createCommand(
            "SELECT SUM(balance_transactions.amount) as summary
                FROM balance_transactions
                INNER JOIN users ON users.id = balance_transactions.user_id
                WHERE users.id={$this->id};"
        );
        $result = $command->queryOne();

        return $result['summary'];
    }

    public function getCustomer() {
        return $this->hasOne(Customer::className(), ['id' => 'parent_id']);
    }
}