<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $username
 * @property string $balance
 * @property int $parent_id
 * @property int $type
 *
 * @property BalanceTransaction[] $balanceTransactions
 */
class User extends ActiveRecord
{
    
    const MANAGER_TYPE = 1;
    const CUSTOMER_TYPE = 2;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['balance'], 'number'],
            [['parent_id', 'type'], 'integer'],
            [['username'], 'string', 'max' => 12],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'balance' => 'Balance',
            'parent_id' => 'Parent ID',
            'type' => 'Type',
        ];
    }

    public static function instantiate($attributes){
        switch($attributes['type']){
            case self::MANAGER_TYPE:
                return new Manager();
                break;
            case self::CUSTOMER_TYPE:
                return new Customer();
                break;
            default:
                return new self;
        }
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBalanceTransactions()
    {
        return $this->hasMany(BalanceTransaction::className(), ['user_id' => 'id']);
    }
}
