<?php


namespace app\commands;

use app\models\BalanceTransaction;
use app\models\Customer;
use app\models\Manager;
use app\models\User;
use yii\console\Controller;
use Faker;
use yii\helpers\ArrayHelper;

class PopulateController extends Controller
{

    /**
     *
     */
    public function actionIndex()
    {
        $customer = Customer::findOne(6);
        var_dump($customer->getBalance());
        /** @var Manager $manager */
        foreach ($customer->getManagers()->all() as $manager) {
            var_dump($manager->getBalance());
        }
    }

    /**
     * @param int $count
     */
    public function actionUsers(int $count = 10) {
        $faker = Faker\Factory::create();
        

        foreach (range(0, $count) as $c) {
            $user = new User();
            $user->username = $faker->userName;

            $types = [
                Manager::TYPE,
                Customer::TYPE
            ];

            $user->type = $types[rand(0,1)];
            if ($user->save(false)) {
                print $c.PHP_EOL;
            }
           
        }
    }

    /**
     *
     */
    public function actionRelations() {
        $managers = Manager::find()->all();

        foreach ($managers as $manager) {
            $customer = Customer::find()
                ->orderBy('RAND()')
                ->limit(1)
                ->one();
            
            /** @var Manager $manager */
            $manager->parent_id = $customer->id;
            
            $manager->save(false);
        }
    }

    public function actionTrans() {
        $managers = Manager::find()->all();
        
        foreach ($managers as $manager) {
            $faker = Faker\Factory::create();
            $balance_transaction = new BalanceTransaction();
            $balance_transaction->amount = $faker->randomFloat(10,null,-50, 50);
            $balance_transaction->user_id = $manager->id;
            $balance_transaction->save(false);
        }
    }



}
